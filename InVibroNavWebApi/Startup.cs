using InVibroNavBackend.Triangulation;
using InVibroNavWebApi.Models;
using InVibroNavWebApi.Models.Abstractions;
using InVibroNavWebApi.Models.Shop;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace InVibroNavWebApi
{
    public class Startup
    {
        private readonly IConfiguration configuration;

        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContextFactory<ModelContext>(options => options.UseSqlite(configuration.GetConnectionString("cs")));
            services.AddScoped<IShopRepository, ShopRepository>();
            services.AddSingleton<AbstractTriangulator>(serviceProvider => 
                ActivatorUtilities.CreateInstance<Triangulation>(serviceProvider, 
                new object[3] { 
                    configuration.GetValue<string>("Triangulation:source"), 
                    configuration.GetValue<int>("Triangulation:seed"),
                    serviceProvider.GetService<Microsoft.Extensions.Logging.ILogger<AbstractTriangulator>>()
                }));
            services.AddHostedService<InitHostedService>();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/error");
                app.UseHsts();
            }

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Location}/{action=GetStores}");
                endpoints.MapControllers();
            });
        }
    }
}
