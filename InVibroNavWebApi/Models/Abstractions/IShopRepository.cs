﻿using InVibroNavWebApi.Models.Shop;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InVibroNavWebApi.Models.Abstractions
{
    public interface IShopRepository : System.IDisposable
    {
        public Task<ShopInformation> GetShopAsync(int shopId);
        public Task<ICollection<ShopInformation>> GetShopsAsync();

        public Task<bool> ContainsAsync(int shopId);

        public Task InsertAsync(ShopInformation shop);
        public Task InsertManyAsync(ICollection<ShopInformation> shops);

        public Task DeleteAsync(int shopId);
        public Task DeleteAsync(ICollection<int> shopIds);
        public Task DeleteAllAsync();

        public Task UpdateShopAsync(int shopId, ShopInformation newShopData);
        public Task UpdateDBShopsCreationDate(System.DateTime creationDate);
        public Task<System.DateTime> GetUTCDBShopsCreationDate();
    }
}
