﻿using InVibroNavBackend;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InVibroNavWebApi.Models.Shop
{
    internal class ShopReader
    {
        public static async Task<ICollection<ShopInformation>> Load(string shopInformationFile, ILogger logger, System.Threading.CancellationToken cancellationToken)
        {
            ICollection<ShopInformation> readShopData = new List<ShopInformation>();
            if (string.IsNullOrWhiteSpace(shopInformationFile))
            {
                shopInformationFile = "<nullEmptyOrWhitespace>";
            }
            try
            {
                readShopData = await Task.Run(async () =>
                {
                    ICollection<ShopInformation> readShopData = new List<ShopInformation>();
                    using (var fileStream = new System.IO.FileStream(shopInformationFile, System.IO.FileMode.Open))
                    {
                        using (var streamReader = new System.IO.StreamReader(fileStream))
                        {
                            await streamReader.ReadLineAsync();
                            while (!streamReader.EndOfStream)
                            {
                                string fileLine = await streamReader.ReadLineAsync();
                                try
                                {
                                    readShopData.Add(ParseShopInformation(fileLine, logger));
                                }
                                catch (ArgumentException) { }
                            }
                        }
                    }
                    return readShopData;
                }, cancellationToken);
                return readShopData;
            }
            catch (Exception e)
            {
                logger.LogError(e, Messages.ERR_PARSING_STORE_DATA_FILE);
                throw new ArgumentException(Messages.ERR_PARSING_STORE_DATA_FILE);
            }
        }

        private static ShopInformation ParseShopInformation(string shopInfoFileLine, ILogger logger)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(shopInfoFileLine))
                {
                    shopInfoFileLine = "<nullEmptyOrWhitespace>";
                }

                string[] lineData = shopInfoFileLine.Split(';');
                if (lineData.Length != 4)
                {
                    throw new ArgumentException(Messages.ERR_INVALID_STORE_FILE_DATA_FORMAT);
                }

                return new ShopInformation()
                {
                    Name = lineData[0],
                    LocationX = double.Parse(lineData[1]),
                    LocationY = double.Parse(lineData[2]),
                    Logo = Convert.FromBase64String(lineData[3])
                };
            }
            catch (Exception e) when (e is ArgumentNullException || e is OverflowException ||e is FormatException || e is IndexOutOfRangeException || e is ArgumentException)
            {
                string errMsg = MessageConstants.BuildMessage(Messages.ERR_PARSING_STORE_FILE_DATA, shopInfoFileLine);
                logger.LogError(e, errMsg);
                throw new ArgumentException(errMsg);
            }
        }
    }
}
