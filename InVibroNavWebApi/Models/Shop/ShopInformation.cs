﻿using System.ComponentModel.DataAnnotations;

namespace InVibroNavWebApi.Models.Shop
{
    public class ShopInformation
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1), MaxLength(255)]
        public string Name { get; set; }

        [Required]
        public byte[] Logo { get; set; }
        public double  LocationX { get; set; }
        public double LocationY { get; set; }

        public ShopInformation()
        {
            Logo = new byte[0];
        }

        public override string ToString()
        {
            string shopAsString = "Id: " + Id + " | Name: ";

            if (string.IsNullOrWhiteSpace(Name))
            {
                shopAsString += "<nullEmptyOrWhitespace>";
            }
            else
            {
                shopAsString += Name;
            }
            shopAsString += " | Logo: " + Logo.ToString();
            shopAsString += " | LocationX: " + LocationX;
            shopAsString += " | LocationY: " + LocationY;
            return shopAsString;
        }
    }
}
