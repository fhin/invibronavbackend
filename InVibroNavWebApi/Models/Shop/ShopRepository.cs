﻿using InVibroNavBackend;
using InVibroNavWebApi.Models.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InVibroNavWebApi.Models.Shop
{
    internal class ShopRepository : IShopRepository
    {
        private readonly IDbContextFactory<ModelContext> dbContextFactory;
        private ModelContext dbContext;
        private readonly ILogger<ShopRepository> logger;

        public ShopRepository(IDbContextFactory<ModelContext> dbContextFactory, ILogger<ShopRepository> logger)
        {
            this.dbContextFactory = dbContextFactory;
            this.logger = logger;
        }

        public async Task<bool> ContainsAsync(int shopId)
        {
            try
            {
                using (var dbContext = dbContextFactory.CreateDbContext())
                {
                    return await dbContext.Shops.FindAsync(shopId) != null;
                }
            }
            catch (System.InvalidOperationException e)
            {
                string errMsg = MessageConstants.BuildMessage(Messages.ERR_FETCHING_STORE_INFORMATION, shopId);
                logger.LogError(e, errMsg);
                throw new System.ArgumentException(errMsg);
            }
        }

        public async Task<bool> ContainsAsync(string shopName)
        {
            if (string.IsNullOrWhiteSpace(shopName)) return false;
            try
            {
                using (var dbContext = dbContextFactory.CreateDbContext())
                {
                    return await dbContext.Shops.FirstOrDefaultAsync(dbShop => dbShop.Name.Equals(shopName)) != null;
                }
            }
            catch (System.InvalidOperationException e)
            {
                string errMsg = MessageConstants.BuildMessage(Messages.ERR_LOOKUP_SHOP_BY_NAME, shopName);
                logger.LogError(e, errMsg);
                throw new System.ArgumentException(errMsg);
            }
        }

        public async Task DeleteAsync(ICollection<int> shopIds)
        {
            try
            {
                using (var dbContext = dbContextFactory.CreateDbContext())
                {
                    foreach (var shopId in shopIds)
                    {
                        if (!(await ContainsAsync(shopId))) continue;
                        ShopInformation shopToDelete = new ShopInformation()
                        {
                            Id = shopId
                        };
                        dbContext.Shops.Remove(shopToDelete);
                    }
                    await dbContext.SaveChangesAsync();
                }
            }
            catch (DbUpdateException e)
            {
                logger.LogError(e, Messages.ERR_DELETING_STORES);
                throw new System.ArgumentException(Messages.ERR_DELETING_STORES);
            }
        }

        public async Task DeleteAsync(int shopId)
        {
            try
            {
                await DeleteAsync(new int[1] { shopId });
            }
            catch (System.ArgumentException e)  
            {
                string errMsg = MessageConstants.BuildMessage(Messages.ERR_DELETING_STORE, shopId);
                logger.LogError(e, errMsg);
                throw new System.ArgumentException(errMsg);
            }
        }

        public async Task DeleteAllAsync()
        {
            try
            {
                using (var dbContext = dbContextFactory.CreateDbContext())
                {
                    foreach (var shopId in dbContext.Shops.Select(dbShop => dbShop.Id))
                    {
                        if (!(await ContainsAsync(shopId))) continue;
                        ShopInformation shopToDelete = new ShopInformation()
                        {
                            Id = shopId
                        };
                        dbContext.Shops.Remove(shopToDelete);
                    }
                    await dbContext.SaveChangesAsync();
                }
            }
            catch (DbUpdateException e)
            {
                logger.LogError(e, Messages.ERR_DELETING_STORES);
                throw new System.ArgumentException(Messages.ERR_DELETING_STORES);
            }
        }

        public async Task<ShopInformation> GetShopAsync(int shopId)
        {
            try
            {
                using (var dbContext = dbContextFactory.CreateDbContext())
                {
                    return await dbContext.Shops.FindAsync(shopId);
                }
            }
            catch (System.Exception e)
            {
                string errMsg = MessageConstants.BuildMessage(Messages.ERR_FETCHING_STORE_INFORMATION, shopId);
                logger.LogError(e, errMsg);
                throw new System.ArgumentException(errMsg);
            }
        }

        public async Task<ICollection<ShopInformation>> GetShopsAsync()
        {
            ICollection<ShopInformation> fetchedInformation = new List<ShopInformation>();
            try
            {
                using (var dbContext = dbContextFactory.CreateDbContext())
                {
                    fetchedInformation = await Task.Run(() => dbContext.Shops.ToArray());
                }
            }
            catch (System.Exception e)
            {
                logger.LogError(e, Messages.ERR_FETCHING_STORES_INFORMATION);
            }
            return fetchedInformation;
        }

        public async Task InsertAsync(ShopInformation shop)
        {
            try
            {
                if (shop == null)
                {
                    throw new System.ArgumentException();
                }
                await InsertManyAsync(new ShopInformation[1] { shop });
            }
            catch (System.ArgumentException e)
            {
                string shopAsString = shop != null ? shop.ToString() : "<null>";
                string errMsg = MessageConstants.BuildMessage(Messages.ERR_FETCHING_STORE_INFORMATION, shopAsString);
                logger.LogError(e, errMsg);
                throw new System.ArgumentException(errMsg);
            }
        }

        public async Task InsertManyAsync(ICollection<ShopInformation> shops)
        {
            try
            {

                using (var dbContext = dbContextFactory.CreateDbContext())
                {
                    foreach (var shop in shops)
                    {
                        if (shop == null || (await ContainsAsync(shop.Id))) return;
                        if (await ContainsAsync(shop.Name))
                        {
                            throw new System.ArgumentException(MessageConstants.BuildMessage(Messages.ERR_DUPLICATE_STORE_NAME, shop.Name));
                        }
                        await dbContext.Shops.AddAsync(shop);
                    }
                    await dbContext.SaveChangesAsync();
                }
            }
            catch (DbUpdateException e)
            {
                logger.LogError(e, Messages.ERR_INSERTING_STORES);
                throw new System.ArgumentException(Messages.ERR_INSERTING_STORES);
            }
        }

        public async Task UpdateShopAsync(int shopId, ShopInformation newShopData)
        {
            if (newShopData == null || newShopData.Name == null) return;
            if (!(await ContainsAsync(shopId))) return;

            try
            {
                using (var dbContext = dbContextFactory.CreateDbContext())
                {
                    ShopInformation dbShop = await dbContext.Shops.FindAsync(shopId);
                    if (!dbShop.Name.Equals(newShopData.Name) && await ContainsAsync(newShopData.Name))
                    {
                        throw new System.ArgumentException(MessageConstants.BuildMessage(Messages.ERR_DUPLICATE_STORE_NAME, newShopData.Name));
                    }

                    dbShop.LocationX = newShopData.LocationX;
                    dbShop.LocationY = newShopData.LocationY;
                    dbShop.Logo = newShopData.Logo;
                    dbShop.Name = newShopData.Name;
                    dbContext.Shops.Update(dbShop);
                    await dbContext.SaveChangesAsync();
                }
            }
            catch (DbUpdateException e)
            {
                string shopAsString = newShopData != null ? newShopData.ToString() : "<null>";
                string errMsg = MessageConstants.BuildMessage(Messages.ERR_UPDATING_STORE, shopAsString);
                logger.LogError(e, errMsg);
                throw new System.ArgumentException(errMsg);
            }
        }

        public async Task UpdateDBShopsCreationDate(System.DateTime creationDate)
        {
            try
            {
                using (var dbContext = dbContextFactory.CreateDbContext())
                {
                    if (creationDate.Kind != System.DateTimeKind.Utc)
                    {
                        creationDate = creationDate.ToUniversalTime();
                    }
                    EntityData dbEntityShopInfo = await dbContext.EntityData.FirstOrDefaultAsync(entityType => entityType.EntityType == EntityType.SHOP_INFORMATION);
                    if (dbEntityShopInfo != null)
                    {
                        dbEntityShopInfo.CreationDate = creationDate;
                        dbContext.EntityData.Update(dbEntityShopInfo);
                    }
                    else
                    {
                        EntityData entityShopInfo = new EntityData()
                        {
                            EntityType = EntityType.SHOP_INFORMATION,
                            CreationDate = creationDate
                        };
                        dbContext.EntityData.Add(entityShopInfo);
                    }
                    await dbContext.SaveChangesAsync();
                }
            }
            catch (System.Exception e)
            {
                logger.LogError(e, Messages.ERR_UPDATING_SHOP_ENTITY_DATE);
                throw new System.ArgumentException(Messages.ERR_UPDATING_SHOP_ENTITY_DATE);
            }
        }

        public async Task<System.DateTime> GetUTCDBShopsCreationDate()
        {
            System.DateTime creationDate = System.DateTime.MinValue;
            try
            {
                using (var dbContext = dbContextFactory.CreateDbContext())
                {
                    EntityData dbEntityShopInfo = await dbContext.EntityData.FirstOrDefaultAsync(entityType => entityType.EntityType == EntityType.SHOP_INFORMATION);
                    if (dbEntityShopInfo != null)
                    {
                        creationDate = dbEntityShopInfo.CreationDate;
                    }
                    creationDate = System.DateTime.SpecifyKind(creationDate, System.DateTimeKind.Utc);
                    return creationDate.ToUniversalTime();
                }
            }
            catch (System.Exception e)
            {
                logger.LogError(e, Messages.ERR_FETCHING_SHOP_ENTITY_DATE);
                throw new System.ArgumentException(Messages.ERR_FETCHING_SHOP_ENTITY_DATE);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (dbContext != null)
                    {
                        dbContext.Dispose();
                        dbContext = null;
                    }
                }
                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~ShopRepository()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
