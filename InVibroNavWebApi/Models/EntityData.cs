﻿using System.ComponentModel.DataAnnotations;

namespace InVibroNavWebApi.Models
{
    public enum EntityType
    {
        SHOP_INFORMATION,
        TRIANGULATION
    }
    public class EntityData
    {
        [Key]
        public int EntityId { get; set; }
        public EntityType EntityType { get; set; }
        [Required]
        public System.DateTime CreationDate { get; set; }
    }
}
