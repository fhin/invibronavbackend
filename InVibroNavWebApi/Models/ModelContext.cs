﻿using InVibroNavWebApi.Models.Shop;
using Microsoft.EntityFrameworkCore;

namespace InVibroNavWebApi.Models
{
    internal class ModelContext : DbContext
    {
        public ModelContext(DbContextOptions<ModelContext> contextOptions)
            : base(contextOptions)
        {
            Database.EnsureCreated();
        }

        public DbSet<ShopInformation> Shops { get; set; }
        public DbSet<EntityData> EntityData { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ShopInformation>().HasIndex(propName => propName.Name).IsUnique();
            builder.Entity<ShopInformation>().Property(entity => entity.Name).IsRequired();

            builder.Entity<EntityData>().Property(entity => entity.EntityType).HasConversion<byte>();
            builder.Entity<EntityData>().HasIndex(propName => propName.EntityType).IsUnique();
            builder.Entity<EntityData>().Property(entity => entity.CreationDate).IsRequired();
        }
    }
}
