﻿using InVibroNavWebApi.Models.Abstractions;
using InVibroNavWebApi.Models.Shop;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace InVibroNavWebApi
{
    // Based on the article: https://andrewlock.net/running-async-tasks-on-app-startup-in-asp-net-core-3/
    public class InitHostedService : IHostedService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<InitHostedService> _logger;

        public InitHostedService(IServiceProvider serviceProvider, ILogger<InitHostedService> logger)
        {
            _serviceProvider = serviceProvider;
            _logger = logger;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                await InitShopInformation(scope, cancellationToken);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
   
        private async Task InitShopInformation(IServiceScope serviceScope, CancellationToken cancellationToken)
        {
            try
            {
                IConfiguration config = serviceScope.ServiceProvider.GetRequiredService<IConfiguration>();
                string shopInformationFileLocation = config.GetValue<string>("ShopInformation:source");
                ILogger<ShopReader> logger = serviceScope.ServiceProvider.GetRequiredService<ILogger<ShopReader>>();

                ICollection<ShopInformation> shopInfoFromConfigFile = await ShopReader.Load(shopInformationFileLocation, logger, cancellationToken);
                using (var shopRepo = serviceScope.ServiceProvider.GetRequiredService<IShopRepository>())
                {
                    DateTime dbShopCreationDate = await shopRepo.GetUTCDBShopsCreationDate();
                    DateTime currConfigShopDate = System.IO.File.GetLastWriteTimeUtc(shopInformationFileLocation);
                    // Only read shop information from file if write time of file is greater than the date stored in the database
                    if (currConfigShopDate.CompareTo(dbShopCreationDate) == 1)
                    {
                        await shopRepo.UpdateDBShopsCreationDate(currConfigShopDate);
                        await shopRepo.DeleteAllAsync();
                        await shopRepo.InsertManyAsync(shopInfoFromConfigFile);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, Messages.ERR_INIT_SHOP_INFORMATION);
                throw new ArgumentException(Messages.ERR_INIT_SHOP_INFORMATION);
            }
        }
    }
}
