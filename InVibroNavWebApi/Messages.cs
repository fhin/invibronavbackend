﻿namespace InVibroNavWebApi
{
    internal static class Messages
    {
        public static string ERR_INSERTING_STORE = "Error while inserting store {0:s}";
        public static string ERR_INSERTING_STORES = "Error inserting stores";
        public static string ERR_FETCHING_STORES_INFORMATION = "Error while fetching information of stores";
        public static string ERR_FETCHING_STORE_INFORMATION = "Error while fetching information for store {0:s}";
        public static string ERR_DELETING_STORE = "Error while deleting store with id {0:d}";
        public static string ERR_DELETING_STORES = "Error while deleting stores";
        public static string ERR_UPDATING_STORE = "Error updating store {0:s}";
        public static string ERR_DUPLICATE_STORE_NAME = "Can only have one store with name {0:s}";
        public static string ERR_LOOKUP_SHOP_BY_NAME = "Error while trying to lookup information for store {0:s}";

        public static string ERR_INVALID_STORE_FILE_DATA_FORMAT = "Store information must have format <name>;<locationX>;<locationY>;<base64Icon>";
        public static string ERR_PARSING_STORE_FILE_DATA = "Error while parsing store file data {0:s}";
        public static string ERR_PARSING_STORE_DATA_FILE = "Error while parsing store data file";

        public static string ERR_INIT_SHOP_INFORMATION = "Error wile initializing shop information";
        public static string ERR_UPDATING_SHOP_ENTITY_DATE = "Error while updating information for entity type shop";
        public static string ERR_FETCHING_SHOP_ENTITY_DATE = "Error while fetching creation date for entity shop";
    }
}
