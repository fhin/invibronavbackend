﻿using InVibroNavBackend.Pathfinding;
using InVibroNavBackend.Triangulation;
using InVibroNavWebApi.Models.Abstractions;
using InVibroNavWebApi.Models.Shop;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InVibroNavWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationController : ControllerBase
    {
        private readonly ILogger<LocationController> logger;
        private readonly IShopRepository shopRepository;
        private readonly AbstractTriangulator triangulation;

        public LocationController(ILogger<LocationController> logger, IShopRepository shopRepository, AbstractTriangulator triangulation)
        {
            this.logger = logger;
            this.shopRepository = shopRepository;
            this.triangulation = triangulation;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ShopInformation>>> GetStores()
        {
            throw new System.NotImplementedException();
        }

        [HttpGet]
        [Route("/path/{storeIdx:int}/{locationX:double}/{locationY:double}")]
        public async Task<ActionResult<ICollection<Waypoint>>> GetPath(int storeIdx, double locationX, double locationY)
        {
            throw new System.NotImplementedException();
        }
    }
}
