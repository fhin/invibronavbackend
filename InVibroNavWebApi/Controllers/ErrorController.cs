﻿using Microsoft.AspNetCore.Mvc;

namespace InVibroNavWebApi.Controllers
{
    public class ErrorController : ControllerBase
    {
        [Route("/error")]
        public IActionResult HandleError() => Problem();
    }
}
