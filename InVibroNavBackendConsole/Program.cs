﻿using InVibroNavBackend;
using InVibroNavBackend.Pathfinding;
using InVibroNavBackend.Triangulation;
using Microsoft.Extensions.Logging;

namespace InVibroNavBackendConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            ILoggerFactory loggerFactory = new LoggerFactory();
            Triangulation t = new Triangulation(args[0], 4711, loggerFactory.CreateLogger(typeof(Triangulation)));
            //t.Triangulate(4711);
            t.Export(args[1], ExportType.DEBUG);

            IPathFinder pathFinder = new AStarPathfinder(t, loggerFactory.CreateLogger(typeof(AStarPathfinder)));
            pathFinder.FindPath(t.Vertices.Get(0), new InVibroNavBackend.Geometry.Vertex(-3, 8), DistanceMeasure.EUCLIDEAN);

        }
    }
}
