﻿using InVibroNavBackend.Geometry;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace InVibroNavBackend.Triangulation
{
    public struct TriangleSearchStructureEntry
    {
        public bool IsInternalNode { get; set; }
        public int TriangleIdx { get; set; }
        public int[] Children { get; set; }

        public TriangleSearchStructureEntry(int triangleIdx)
        {
            IsInternalNode = false;
            TriangleIdx = triangleIdx;
            Children = new int[3] { Constants.NOT_SET_VERTEX_INDEX, Constants.NOT_SET_VERTEX_INDEX, Constants.NOT_SET_VERTEX_INDEX };
        }
    }

    public struct TriangleSearchTree
    {
        public IList<TriangleSearchStructureEntry> SearchEntries { get; set; }

        public TriangleSearchTree(TriangleSearchStructureEntry headEntry)
        {
            SearchEntries = new List<TriangleSearchStructureEntry>();
            SearchEntries.Add(headEntry);
        }
    }

    public class Triangulation : AbstractTriangulator
    {
        private TriangleSearchTree searchTree;

        public Triangulation(string config, int seed, ILogger Logger)
            : base(config, seed, Logger)
        {
            Triangulate(seed);
        }

        protected override void Triangulate(int seed)
        {
            try
            {
                TriangleSearchStructureEntry searchHeadEntry = new TriangleSearchStructureEntry(0);
                searchTree = new TriangleSearchTree(searchHeadEntry);
                int currTriangleCnt = 1;
                IEnumerable<int> indizes = Enumerable.Range(0, Vertices.GetEntryCount());
                int idxHighestVertex = FindHighestVertex(Vertices.GetEntryCount());

                ICollection<int> vertexIdxs;
                if (seed == 0)
                {
                    vertexIdxs = indizes.Where(index => index != idxHighestVertex).ToArray();
                }
                else
                {
                    vertexIdxs = PermutateIndizes(indizes.Where(index => index != idxHighestVertex).ToArray(), seed);
                }
                CreateBoundingTriangle(idxHighestVertex);

                foreach (var vertexIdx in vertexIdxs)
                {
                    Vertex currVertex = Vertices.Get(vertexIdx);
                    int enclosingTriangleSearchIdx = FindEnclosingTriangle(vertexIdx, 0);
                    TriangleSearchStructureEntry enclosingTriangleEntry = searchTree.SearchEntries[enclosingTriangleSearchIdx];
                    int enclosingTriangleIdx = enclosingTriangleEntry.TriangleIdx;
                    Triangle enclosingTriangle = Triangles.Get(enclosingTriangleIdx);
                    //Vertex[] enclosingVertices = enclosingTriangle.Vertices.Select(vertexIdx => Vertices.Get(vertexIdx)).ToArray();

                    int vertexInsideOfTriangle = enclosingTriangle.Contains(vertexIdx, Vertices, Edges);
                    //int inCircleTestResult = DoesVertexLieInsideOfTriangle(enclosingTriangleIdx, vertexIdx);
                    if (vertexInsideOfTriangle == Constants.VERTEX_INSIDE_TRIANGLE)
                    {
                        // Add three new edge/triangles from the vertex to all vertices of the enclosing triangle
                        //        p_2
                        //     /   |  \
                        //    /   p_n  \
                        //   /  /     \ \
                        //  p_0  ------- p_1
                        int p0, p1, p2, pn;
                        int[] triangleVertices = enclosingTriangle.Vertices.ToArray();
                        p0 = triangleVertices[0];
                        p1 = triangleVertices[1];
                        p2 = triangleVertices[2];
                        pn = vertexIdx;

                        int eP0P1Idx = enclosingTriangle.GetEdgeIdx(p0, p1, Edges);
                        Edge eP0P1 = Edges.Get(eP0P1Idx);

                        int eP1P2Idx = enclosingTriangle.GetEdgeIdx(p1, p2, Edges);
                        Edge eP1P2 = Edges.Get(eP1P2Idx);

                        int eP2P0Idx = enclosingTriangle.GetEdgeIdx(p2, p0, Edges);
                        Edge eP2P0 = Edges.Get(eP2P0Idx);

                        Triangle p0p1pn = CreateTriangle(enclosingTriangleIdx, p0, p1, pn, true);
                        Triangle p1p2pn = CreateTriangle(currTriangleCnt++, p1, p2, pn);
                        Triangle p2p0pn = CreateTriangle(currTriangleCnt++, p2, p0, pn);

                        // Update triangle search structure
                        enclosingTriangleEntry.IsInternalNode = true;
                        int searchTreeCount = searchTree.SearchEntries.Count;
                        searchTree.SearchEntries.Add(new TriangleSearchStructureEntry(p0p1pn.Idx));
                        searchTree.SearchEntries.Add(new TriangleSearchStructureEntry(p1p2pn.Idx));
                        searchTree.SearchEntries.Add(new TriangleSearchStructureEntry(p2p0pn.Idx));
                        enclosingTriangleEntry.Children[0] = searchTreeCount++;
                        enclosingTriangleEntry.Children[1] = searchTreeCount++;
                        enclosingTriangleEntry.Children[2] = searchTreeCount;
                        searchTree.SearchEntries[enclosingTriangleSearchIdx] = enclosingTriangleEntry;

                        //currTriangleCnt--;
                        //RemoveTriangleAndEdges(enclosingTriangle);
                        UpdateAdjacentTriangleForExistingEdge(enclosingTriangle, eP0P1, p0p1pn.Idx, p0p1pn.GetEdgeIdx(p0, p1, Edges));
                        UpdateAdjacentTriangleForExistingEdge(enclosingTriangle, eP1P2, p1p2pn.Idx, p1p2pn.GetEdgeIdx(p1, p2, Edges));
                        UpdateAdjacentTriangleForExistingEdge(enclosingTriangle, eP2P0, p2p0pn.Idx, p2p0pn.GetEdgeIdx(p2, p0, Edges));
                        //RemoveTriangleAndEdges(enclosingTriangle);

                        p0p1pn.SetAdjacentTriangleForEdge(p0p1pn.GetEdgeIdx(p1, pn, Edges), p1p2pn.Idx);
                        p0p1pn.SetAdjacentTriangleForEdge(p0p1pn.GetEdgeIdx(pn, p0, Edges), p2p0pn.Idx);
                        //Triangles.Update(Triangles.Get(p0p1pn.Idx), p0p1pn);

                        p1p2pn.SetAdjacentTriangleForEdge(p1p2pn.GetEdgeIdx(p2, pn, Edges), p2p0pn.Idx);
                        p1p2pn.SetAdjacentTriangleForEdge(p1p2pn.GetEdgeIdx(pn, p1, Edges), p0p1pn.Idx);
                        //Triangles.Update(Triangles.Get(p1p2pn.Idx), p1p2pn);

                        p2p0pn.SetAdjacentTriangleForEdge(p2p0pn.GetEdgeIdx(p0, pn, Edges), p0p1pn.Idx);
                        p2p0pn.SetAdjacentTriangleForEdge(p2p0pn.GetEdgeIdx(pn, p2, Edges), p1p2pn.Idx);
                        //Triangles.Update(Triangles.Get(p2p0pn.Idx), p2p0pn);

                        LegalizeEdge(p0p1pn.Idx, p0p1pn.GetEdgeIdx(p0, p1, Edges));
                        LegalizeEdge(p1p2pn.Idx, p1p2pn.GetEdgeIdx(p1, p2, Edges));
                        LegalizeEdge(p2p0pn.Idx, p2p0pn.GetEdgeIdx(p2, p0, Edges));
                    }
                    else if (vertexInsideOfTriangle == Constants.VERTEX_ON_BORDER_TRIANGLE)
                    {
                        //currTriangleCnt -= 2;
                        // Vertex lies on one of the edges of the enclosing triangles
                        //
                        //             p_3
                        //           /  |   \
                        //          /   |    \
                        //       p_0 ==p_n== p_2
                        //          \   |   /
                        //           \  |  /
                        //             p_1 
                        //
                        int enclosingEdgeIdx = enclosingTriangle.GetEdgeContainingVertex(currVertex, Edges, Vertices);
                        Edge enclosingEdge = Edges.Get(enclosingEdgeIdx);
                        Edges.Remove(enclosingEdge);

                        int adjacentTriangleIdx = enclosingTriangle.AdjacentTriangleForEdge(enclosingEdgeIdx);
                        Triangle adjacentTriangle = Triangles.Get(adjacentTriangleIdx);

                        int p0, p1, p2, p3, pn;
                        p2 = enclosingEdge.StartVertexIdx;
                        p1 = enclosingEdge.EndVertexIdx;
                        // TODO:
                        p0 = enclosingTriangle
                            .Edges
                            .Select(edgeIdx => Edges.Get(edgeIdx))
                            .First(edge => edge.StartVertexIdx != enclosingEdge.StartVertexIdx)
                            .StartVertexIdx;
                        p3 = adjacentTriangle
                            .Edges
                            .Select(edgeIdx => Edges.Get(edgeIdx))
                            .First(edge => edge.StartVertexIdx != enclosingEdge.StartVertexIdx)
                            .StartVertexIdx;
                        pn = vertexIdx;

                        int eEdgeP0P1Idx = enclosingTriangle.GetEdgeIdx(p0, p1, Edges);
                        Edge eEdgeP0P1 = Edges.Get(eEdgeP0P1Idx);

                        int aEdgeP1P2Idx = adjacentTriangle.GetEdgeIdx(p1, p2, Edges);
                        Edge aEdgeP1P2 = Edges.Get(aEdgeP1P2Idx);

                        int aEdgeP2P3Idx = adjacentTriangle.GetEdgeIdx(p2, p3, Edges);
                        Edge aEdgeP2P3 = Edges.Get(aEdgeP2P3Idx);

                        int eEdgeP3P0Idx = enclosingTriangle.GetEdgeIdx(p3, p0, Edges);
                        Edge eEdgeP3P0 = Edges.Get(eEdgeP3P0Idx);

                        Triangle p0p1pn = CreateTriangle(enclosingTriangleIdx, p0, p1, pn, true);
                        Triangle p1p2pn = CreateTriangle(adjacentTriangleIdx, p1, p2, pn, true);
                        Triangle p2p3pn = CreateTriangle(currTriangleCnt++, p2, p3, pn);
                        Triangle p3p0pn = CreateTriangle(currTriangleCnt++, p3, p0, pn);

                        // Update search structure
                        int adjacentTriangleSearchIdx = FindEnclosingTriangle(adjacentTriangle.Vertices.First(), 0);
                        TriangleSearchStructureEntry adjacentTriangleEntry = searchTree.SearchEntries[adjacentTriangleSearchIdx];

                        int searchTreeCount = searchTree.SearchEntries.Count;
                        searchTree.SearchEntries.Add(new TriangleSearchStructureEntry(p0p1pn.Idx));
                        searchTree.SearchEntries.Add(new TriangleSearchStructureEntry(p3p0pn.Idx));
                        enclosingTriangleEntry.IsInternalNode = true;
                        enclosingTriangleEntry.Children[0] = searchTreeCount++;
                        enclosingTriangleEntry.Children[1] = searchTreeCount++;
                        searchTree.SearchEntries[enclosingTriangleSearchIdx] = enclosingTriangleEntry;

                        searchTreeCount = searchTree.SearchEntries.Count;
                        searchTree.SearchEntries.Add(new TriangleSearchStructureEntry(p1p2pn.Idx));
                        searchTree.SearchEntries.Add(new TriangleSearchStructureEntry(p2p3pn.Idx));
                        adjacentTriangleEntry.IsInternalNode = true;
                        adjacentTriangleEntry.Children[0] = searchTreeCount++;
                        adjacentTriangleEntry.Children[1] = searchTreeCount;
                        searchTree.SearchEntries[adjacentTriangleSearchIdx] = adjacentTriangleEntry;

                        //RemoveTriangleAndEdges(enclosingTriangle);
                        //RemoveTriangleAndEdges(adjacentTriangle);

                        UpdateAdjacentTriangleForExistingEdge(enclosingTriangle, eEdgeP0P1, p0p1pn.Idx, p0p1pn.GetEdgeIdx(p0, p1, Edges));
                        UpdateAdjacentTriangleForExistingEdge(enclosingTriangle, eEdgeP3P0, p3p0pn.Idx, p3p0pn.GetEdgeIdx(p3, p0, Edges));
                        UpdateAdjacentTriangleForExistingEdge(adjacentTriangle, aEdgeP1P2, p1p2pn.Idx, p1p2pn.GetEdgeIdx(p1, p2, Edges));
                        UpdateAdjacentTriangleForExistingEdge(adjacentTriangle, aEdgeP2P3, p2p3pn.Idx, p2p3pn.GetEdgeIdx(p2, p3, Edges));

                        p0p1pn.SetAdjacentTriangleForEdge(p0p1pn.GetEdgeIdx(p1, pn, Edges), p1p2pn.Idx);
                        p0p1pn.SetAdjacentTriangleForEdge(p0p1pn.GetEdgeIdx(pn, p0, Edges), p3p0pn.Idx);
                        //Triangles.Update(p0p1pn, p0p1pn);

                        p1p2pn.SetAdjacentTriangleForEdge(p1p2pn.GetEdgeIdx(p2, pn, Edges), p2p3pn.Idx);
                        p1p2pn.SetAdjacentTriangleForEdge(p1p2pn.GetEdgeIdx(pn, p1, Edges), p0p1pn.Idx);
                        //Triangles.Update(p1p2pn, p1p2pn);

                        p2p3pn.SetAdjacentTriangleForEdge(p2p3pn.GetEdgeIdx(p3, pn, Edges), p3p0pn.Idx);
                        p2p3pn.SetAdjacentTriangleForEdge(p2p3pn.GetEdgeIdx(pn, p2, Edges), p1p2pn.Idx);
                        //Triangles.Update(p2p3pn, p2p3pn);

                        p3p0pn.SetAdjacentTriangleForEdge(p3p0pn.GetEdgeIdx(p0, pn, Edges), p0p1pn.Idx);
                        p3p0pn.SetAdjacentTriangleForEdge(p3p0pn.GetEdgeIdx(pn, p3, Edges), p2p3pn.Idx);
                        //Triangles.Update(p3p0pn, p3p0pn);

                        LegalizeEdge(p0p1pn.Idx, p0p1pn.GetEdgeIdx(p0, p1, Edges));
                        LegalizeEdge(p1p2pn.Idx, p1p2pn.GetEdgeIdx(p1, p2, Edges));
                        LegalizeEdge(p2p3pn.Idx, p2p3pn.GetEdgeIdx(p2, p3, Edges));
                        LegalizeEdge(p3p0pn.Idx, p3p0pn.GetEdgeIdx(p3, p0, Edges));
                    }
                    else
                    {
                        // TODO: Error message and logging
                        throw new System.InvalidOperationException();
                    }
                }
            }
            catch (System.Exception e)
            {
                string errMsg = MessageConstants.BuildMessage(MessageConstants.ERR_FAILED_TRIANGULATION, e.Message);
                Logger.LogError(errMsg);
                throw new System.InvalidOperationException(errMsg, e);
            }
        }

        public override void Export(string exportLocation, ExportType exportType)
        {
            if (string.IsNullOrWhiteSpace(exportLocation))
            {
                exportLocation = "<nullEmptyOrWhitespace>";
            }
            try
            {
                System.IO.FileAttributes fileAttributes = System.IO.File.GetAttributes(exportLocation);
                if (fileAttributes.HasFlag(System.IO.FileAttributes.Directory))
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.AppendLine(Constants.DEF_CONFIG_LINE_PREFIX + " " + Vertices.GetEntryCount() + " " + Edges.GetEntryCount());

                    foreach (Vertex v in Vertices.GetAll())
                    {
                        sb.AppendLine(v.ToString());
                    }
                    foreach (Edge e in Edges.GetAll())
                    {
                        sb.AppendLine(e.ToString());
                    }
                    foreach (Triangle t in Triangles.GetAll())
                    {
                        sb.AppendLine(exportType == ExportType.DEBUG ? t.ToDebugString() : t.ToString());
                    }

                    string exportFileName = "triangulationDump.txt";
                    string exportPath = System.IO.Path.Combine(exportLocation, exportFileName);
                    using (var fileStream = System.IO.File.Create(exportPath))
                    {
                        using (var streamWriter = new System.IO.StreamWriter(fileStream))
                        {
                            streamWriter.Write(sb.ToString());
                        }
                    }
                }
                else 
                {
                    string errMsg = MessageConstants.BuildMessage(MessageConstants.ERR_EXPORT_LOCATION_NOT_DIRECTORY, exportLocation);
                    Logger.LogError(errMsg);
                    throw new System.ArgumentException(errMsg);
                }
            }
            catch (System.Exception e)
            {
                Logger.LogError(MessageConstants.ERR_EXPORTING_TRIANGULATION);
                throw new System.ArgumentException(MessageConstants.ERR_EXPORTING_TRIANGULATION, e);
            }
        }

        public override int FindEnclosingTriangle(Vertex vertex)
        {
            int enclosingTriangleSearchEntry = FindEnclosingTriangle(vertex, 0);
            try
            {
                return searchTree.SearchEntries[enclosingTriangleSearchEntry].TriangleIdx;
            }
            catch (System.IndexOutOfRangeException e)
            {
                string vertexAsString = "<nullOrUndefind>";
                try
                {
                    vertexAsString = vertex.ToString();
                }
                catch (System.NullReferenceException) { }
                string errMsg = MessageConstants.BuildMessage(MessageConstants.ERR_NO_ENCLOSING_TRIANGLE_FOR_POINT, vertexAsString);
                Logger.LogError(e, errMsg);
                throw new System.ArgumentException(errMsg);
            }
        }

        private void CreateBoundingTriangle(int firstVertexIdx)
        {
            // TODO: Correctly define vertices of bounding triangle
            Vertex boundingVertex_0 = new Vertex(-10, 10);
            Vertex boundingVertex_1 = new Vertex(10, -10);

            int boundingVertex0Idx = Vertices.InsertEntity(boundingVertex_0);
            int boundingVertex1Idx = Vertices.InsertEntity(boundingVertex_1);
            int boundingVertex2Idx = firstVertexIdx;

            Triangle boundingTriangle = new Triangle(Triangles.GetEntryCount());
            boundingTriangle.Vertices.Add(boundingVertex0Idx);
            boundingTriangle.Vertices.Add(boundingVertex1Idx);
            boundingTriangle.Vertices.Add(boundingVertex2Idx);

            int edgeCount = Edges.GetEntryCount();
            Edge boundEdge1 = new Edge(boundingVertex0Idx, boundingVertex1Idx);
            boundingTriangle.Edges.Add(edgeCount);
            boundingTriangle.SetAdjacentTriangleForEdge(edgeCount++, Constants.UNBOUNDED_FACE_INDEX);

            Edge boundEdge2 = new Edge(boundingVertex1Idx, boundingVertex2Idx);
            boundingTriangle.Edges.Add(edgeCount);
            boundingTriangle.SetAdjacentTriangleForEdge(edgeCount++, Constants.UNBOUNDED_FACE_INDEX);

            Edge boundEdge3 = new Edge(boundingVertex2Idx, boundingVertex0Idx);
            boundingTriangle.Edges.Add(edgeCount);
            boundingTriangle.SetAdjacentTriangleForEdge(edgeCount, Constants.UNBOUNDED_FACE_INDEX);

            Edges.InsertEntity(boundEdge1);
            Edges.InsertEntity(boundEdge2);
            Edges.InsertEntity(boundEdge3);
            Triangles.InsertEntity(boundingTriangle);
        }

        private int FindEnclosingTriangle(Vertex vertex, int searchEntryIdx)
        {
            TriangleSearchStructureEntry currEntry = searchTree.SearchEntries[searchEntryIdx];
            if (currEntry.IsInternalNode)
            {
                foreach (var childEntry in currEntry.Children)
                {
                    int foundSearchNodeIdx = FindEnclosingTriangle(vertex, childEntry);
                    if (foundSearchNodeIdx == Constants.UNDEFINED_TRIANGLE_INDEX) continue;
                    return searchTree.SearchEntries[childEntry].IsInternalNode ? foundSearchNodeIdx : childEntry;
                }
            }
            else
            {
                if (currEntry.TriangleIdx == Constants.UNDEFINED_TRIANGLE_INDEX)
                {
                    return Constants.UNDEFINED_TRIANGLE_INDEX;
                }
                Triangle currTriangle = Triangles.Get(currEntry.TriangleIdx);
                Vertex[] currTriangleVertices = currTriangle.Vertices.Select(vertexIdx => Vertices.Get(vertexIdx)).ToArray();
                return currTriangle.Contains(vertex, Vertices, Edges) != Constants.VERTEX_OUTSIDE_TRIANGLE ? searchEntryIdx : Constants.UNDEFINED_TRIANGLE_INDEX;
            }
            return Constants.UNDEFINED_TRIANGLE_INDEX;
        }

        private int FindEnclosingTriangle(int vertexIdx, int searchEntryIdx)
        {
            return FindEnclosingTriangle(Vertices.Get(vertexIdx), searchEntryIdx);
        }

        private void RemoveTriangleAndEdges(Triangle oldTriangle)
        {
            Triangles.Remove(oldTriangle);
            // TODO: Use ref-count to check if a edge can be safely deleted
            /*
            foreach (var edgeIdx in oldTriangle.Edges)
            {
                Edges.Remove(Edges.Get(edgeIdx));
            }
            */
        }

        private void UpdateAdjacentTriangleForExistingEdge(Triangle oldTriangle, Edge oldEdge, int newTriangleIdx, int newEdgeIdx)
        {
            int oldEdgeIdx = oldTriangle.GetEdgeIdx(oldEdge.StartVertexIdx, oldEdge.EndVertexIdx, Edges);

            int adjacentTriangleIdx = oldTriangle.AdjacentTriangleForEdge(oldEdgeIdx);
            if (adjacentTriangleIdx != Constants.UNBOUNDED_FACE_INDEX)
            {
                Triangle adjacentTriangle = Triangles.Get(adjacentTriangleIdx);
                int adjacentEdgeIdx = adjacentTriangle.GetEdgeIdx(oldEdge.StartVertexIdx, oldEdge.EndVertexIdx, Edges);
                adjacentTriangle.SetAdjacentTriangleForEdge(adjacentEdgeIdx, newTriangleIdx);
            }
            Triangle newTriangle = Triangles.Get(newTriangleIdx);
            newTriangle.SetAdjacentTriangleForEdge(newEdgeIdx, adjacentTriangleIdx);
        }

        private Triangle CreateTriangle(int triangleIdx, int vertex1, int vertex2, int vertex3)
        {
            return CreateTriangle(triangleIdx, vertex1, vertex2, vertex3, false);
        }

        private Triangle CreateTriangle(int triangleIdx, int vertex1, int vertex2, int vertex3, bool replaceExisiting)
        {
            int edgev1v2 = Edges.InsertEntity(new Edge(vertex1, vertex2));
            int edgev2v3 = Edges.InsertEntity(new Edge(vertex2, vertex3));
            int edgev3v1 = Edges.InsertEntity(new Edge(vertex3, vertex1));

            Triangle newTriangle = new Triangle(triangleIdx)
            {
                Vertices = new HashSet<int>(3) { vertex1, vertex2, vertex3 },
                Edges = new HashSet<int>(3) { edgev1v2, edgev2v3, edgev3v1 }
            };
            newTriangle.SetAdjacentTriangleForEdge(edgev1v2, Constants.UNDEFINED_TRIANGLE_INDEX);
            newTriangle.SetAdjacentTriangleForEdge(edgev2v3, Constants.UNDEFINED_TRIANGLE_INDEX);
            newTriangle.SetAdjacentTriangleForEdge(edgev3v1, Constants.UNDEFINED_TRIANGLE_INDEX);
            if (replaceExisiting)
            {
                Triangles.Update(Triangles.Get(triangleIdx), newTriangle);
            }
            else
            {
                Triangles.InsertEntity(newTriangle);
            }
            return newTriangle;
        }

        private int DoesVertexLieInsideOfTriangle(int triangleIdx, int vertexIdx)
        {
            Triangle t = Triangles.Get(triangleIdx);
            Vertex[] vertices = t.Vertices.Select(vertexIdx => Vertices.Get(vertexIdx)).ToArray();
            return t.VertexInTriangleSpannedCircle(Vertices.Get(vertexIdx), vertices[0], vertices[1], vertices[2]);
        }

        private bool IsEdgeLegal(int triangleIdx, int edgeIdx)
        {
            Triangle t = Triangles.Get(triangleIdx);
            int adjacentTriangleIdx = t.AdjacentTriangleForEdge(edgeIdx);
            if (adjacentTriangleIdx == Constants.UNBOUNDED_FACE_INDEX) return true;
            Triangle adjacentTriangle = Triangles.Get(adjacentTriangleIdx);
            int adjacentDistinctVertexIdx = adjacentTriangle.Vertices.Single(vertexIdx => !t.Vertices.Contains(vertexIdx));
            return DoesVertexLieInsideOfTriangle(triangleIdx, adjacentDistinctVertexIdx) != 1;

        }

        /*
         *  ps ==  pk_2
         *   | \    |
         *   |  \   |
         *   |   \  |
         *  pk_1 == p_e
         *  
         *  Legalize edge p_s -> p_e
         *  
         *  *  ps == pk_2
         *   |      /  |
         *   |    /    |
         *   |  /      |
         *  pk_1 == p_e
         */

        private void LegalizeEdge(int triangleIdx, int edgeIdx)
        {
            if (IsEdgeLegal(triangleIdx, edgeIdx))
            {
                return;
            }
            Edge edgeToRemove = Edges.Get(edgeIdx);
            Triangle t = Triangles.Get(triangleIdx);
            Triangle adjacentTriangle = Triangles.Get(t.AdjacentTriangleForEdge(edgeIdx));

            int ps = edgeToRemove.StartVertexIdx;
            int pe = edgeToRemove.EndVertexIdx;
            int pk_1 = t.Vertices.Single(vertexIdx => vertexIdx != ps && vertexIdx != pe);
            int pk_2 = adjacentTriangle.Vertices.Single(vertexIdx => vertexIdx != ps && vertexIdx != pe);

            int edge_ps_pk_1 = t.GetEdgeIdx(ps, pk_1, Edges);
            int edge_pe_pk_2 = adjacentTriangle.GetEdgeIdx(pe, pk_2, Edges);
            int edge_pk_2_ps = adjacentTriangle.GetEdgeIdx(pk_2, ps, Edges);

            int leftAdjacentTriangle_edge_ps_pk_1 = t.AdjacentTriangleForEdge(edge_ps_pk_1);
            int rightAdjacentTriangle_Edge_pe_pk_2 = adjacentTriangle.AdjacentTriangleForEdge(edge_pe_pk_2);
            
            t.Vertices.Remove(ps);
            t.Edges.Remove(edgeIdx);
            t.Edges.Remove(edge_ps_pk_1);

            t.Vertices.Add(pk_2);
            int edge_pk_1_pk_2 = Edges.InsertEntity(new Edge(pk_1, pk_2));
            t.Edges.Add(edge_pk_1_pk_2);
            t.Edges.Add(edge_pe_pk_2);
            t.UpdateAdjacentTriangleForEdge(edgeIdx, edge_pk_1_pk_2, adjacentTriangle.Idx);
            t.UpdateAdjacentTriangleForEdge(edge_ps_pk_1, edge_pe_pk_2, adjacentTriangle.AdjacentTriangleForEdge(edge_pe_pk_2));
            //UpdateAdjacentTriangleForExistingEdge(t, Edges.Get(edge_ps_pk_1), adjacentTriangle.Idx, edge_ps_pk_1);
            if (leftAdjacentTriangle_edge_ps_pk_1 != Constants.UNBOUNDED_FACE_INDEX)
            {
                Triangle t_leftAdjacentTriangle = Triangles.Get(leftAdjacentTriangle_edge_ps_pk_1);
                t_leftAdjacentTriangle.SetAdjacentTriangleForEdge(edge_ps_pk_1, adjacentTriangle.Idx);
            }

            adjacentTriangle.Vertices.Remove(pe);
            adjacentTriangle.Edges.Remove(edge_pe_pk_2);
            adjacentTriangle.Edges.Remove(edgeIdx);

            adjacentTriangle.Vertices.Add(pk_1);
            adjacentTriangle.Edges.Add(edge_ps_pk_1);
            adjacentTriangle.Edges.Add(edge_pk_1_pk_2);
            adjacentTriangle.UpdateAdjacentTriangleForEdge(edge_pe_pk_2, edge_pk_1_pk_2, t.Idx);
            adjacentTriangle.UpdateAdjacentTriangleForEdge(edgeIdx, edge_ps_pk_1, leftAdjacentTriangle_edge_ps_pk_1);
            //UpdateAdjacentTriangleForExistingEdge(adjacentTriangle, Edges.Get(edge_pe_pk_2), t.Idx, edge_pe_pk_2);
            if (rightAdjacentTriangle_Edge_pe_pk_2 != Constants.UNBOUNDED_FACE_INDEX)
            {
                Triangle adjT_rightAdjacentTriangle = Triangles.Get(rightAdjacentTriangle_Edge_pe_pk_2);
                adjT_rightAdjacentTriangle.SetAdjacentTriangleForEdge(edge_pe_pk_2, t.Idx);
            }

            LegalizeEdge(t.Idx, edge_pe_pk_2);
            LegalizeEdge(adjacentTriangle.Idx, edge_pk_2_ps);
        }

        /** Permutate given indices according to algorithm:
         * https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle 
         **/
        private ICollection<int> PermutateIndizes(IList<int> indizes, int seed)
        {
            System.Random rdm = new System.Random(seed);
            int n = indizes.Count;
            for (int idx = n-1; idx >= 1; idx--)
            {
                int oIdx = rdm.Next(0, idx);
                int backupElem = indizes[idx];
                indizes[idx] = indizes[oIdx];
                indizes[oIdx] = backupElem;
            }
            return indizes;
        }
    
        private int FindHighestVertex(int numIndizes)
        {
            // TODO: Error message
            if (numIndizes == 0) throw new System.ArgumentException();

            var highestIndex = 0;
            Vertex highestVertex = Vertices.Get(highestIndex);

            for (int index = highestIndex+1; index < numIndizes; index++)
            {
                Vertex currVertex = Vertices.Get(index);
                if (currVertex.CompareTo(highestVertex) == -1)
                {
                    highestIndex = index;
                    highestVertex = Vertices.Get(highestIndex);
                }
            }
            return highestIndex;
        }
    }
}
