﻿using InVibroNavBackend.Geometry;
using Microsoft.Extensions.Logging;

namespace InVibroNavBackend.Triangulation
{
    public enum ExportType
    {
        MINIMAL,
        STANDARD,
        DEBUG
    }

    public abstract class AbstractTriangulator
    {
        public GeometricEntityContainer<Vertex> Vertices { get; set; }
        public GeometricEntityContainer<Edge> Edges { get; set; }
        public GeometricEntityContainer<Triangle> Triangles { get; set; }
        public ILogger Logger { get; }

        public AbstractTriangulator(string config, int seed, ILogger logger)
        {
            Logger = logger;
            Vertices = new GeometricEntityContainer<Vertex>(logger);
            Edges = new GeometricEntityContainer<Edge>(logger);
            Triangles = new GeometricEntityContainer<Triangle>(logger);

            Init(config);
        }
        protected void Init(string config)
        {
            try
            {
                ConfigReader configReader = new ConfigReader(Logger);
                (Vertices, Edges) = configReader.Read(config);
            }
            catch (System.ArgumentException e)
            {
                string errMsg = MessageConstants.ERR_INIT_TRIANGULATOR;
                Logger.LogError(e, errMsg);
                throw new System.ArgumentException(errMsg, e);
            }
        }

        protected abstract void Triangulate(int seed);
        public abstract void Export(string exportLocation, ExportType exportType);
        public abstract int FindEnclosingTriangle(Vertex vertex);
    }
}
