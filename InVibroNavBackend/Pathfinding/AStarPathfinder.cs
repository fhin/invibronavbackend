﻿using InVibroNavBackend.Geometry;
using InVibroNavBackend.Triangulation;
using Microsoft.Extensions.Logging;
using Priority_Queue;
using System.Collections.Generic;

namespace InVibroNavBackend.Pathfinding
{
    public class AStarPathfinder : IPathFinder
    {
        private readonly AbstractTriangulator triangulation;
        private readonly ILogger logger;
        private FastPriorityQueue<SearchNode> openList;
        private IDictionary<int, SearchNodeContent> searchContentLookupDict;

        public AStarPathfinder(AbstractTriangulator triangulation, ILogger logger)
        {
            this.triangulation = triangulation;
            this.logger = logger;
            openList = new FastPriorityQueue<SearchNode>(triangulation.Triangles.GetEntryCount());
            searchContentLookupDict = new Dictionary<int, SearchNodeContent>(triangulation.Triangles.GetEntryCount());
        }

        public ICollection<Waypoint> FindPath(Vertex startVertex, Vertex endVertex, DistanceMeasure distanceMeasure)
        {
            openList.Clear();
            searchContentLookupDict.Clear();
            IList<Waypoint> waypoints = new List<Waypoint>();
            try
            {
                int startVertexTriangleIdx = triangulation.FindEnclosingTriangle(startVertex);
                int endVertexTriangleIdx = triangulation.FindEnclosingTriangle(endVertex);

                if (startVertexTriangleIdx == Constants.UNDEFINED_TRIANGLE_INDEX)
                {
                    throw new System.ArgumentException(MessageConstants.ERR_NO_ENCLOSING_TRIANGLE_FOR_POINT, startVertex.ToString());
                }
                if (endVertexTriangleIdx == Constants.UNDEFINED_TRIANGLE_INDEX)
                {
                    throw new System.ArgumentException(MessageConstants.ERR_NO_ENCLOSING_TRIANGLE_FOR_POINT, endVertex.ToString());
                }
                if (!ExistsPath(endVertexTriangleIdx, startVertexTriangleIdx, distanceMeasure))
                {
                    throw new System.ArgumentException(MessageConstants.ERR_NO_PATH_FOUND);
                }

                int currSearchEntryIdx = startVertexTriangleIdx;
                Triangle currTriangle = triangulation.Triangles.Get(currSearchEntryIdx);

                // Add waypoint from start vertex to centroid of first triangle on search path
                (double startTriangleMidX, double startTriangleMidY) = CalculateTriangleMidPoint(currTriangle);
                Waypoint startToFirstMidpoint = CreateWaypoint(startVertex.X, startVertex.Y, startTriangleMidX, startTriangleMidY);
                if (startToFirstMidpoint != null)
                {
                    waypoints.Add(startToFirstMidpoint);
                } 

                while (currSearchEntryIdx != endVertexTriangleIdx)
                {
                    SearchNodeContent currPathSearchNode = searchContentLookupDict[currSearchEntryIdx];
                    Triangle nextTriangle = triangulation.Triangles.Get(currPathSearchNode.PrevIdx);

                    (double currTriangleMidX, double currTriangleMidY) = CalculateTriangleMidPoint(currTriangle);
                    (double nextTriangleMidX, double nextTriangleMidY) = CalculateTriangleMidPoint(nextTriangle);

                    Waypoint currWaypoint = CreateWaypoint(currTriangleMidX, currTriangleMidY, nextTriangleMidX, nextTriangleMidY);
                    if (currWaypoint != null)
                    {
                        waypoints.Add(currWaypoint);
                    }
                    currSearchEntryIdx = currPathSearchNode.PrevIdx;
                    currTriangle = nextTriangle;
                }

                // Add waypoint from centroid of last triangle on search path to end vertex
                (double endTriangleMidX, double endTriangleMidY) = CalculateTriangleMidPoint(currTriangle);
                Waypoint lastMidpointToEndV = CreateWaypoint(endTriangleMidX, endTriangleMidY, endVertex.X, endVertex.Y);
                if (lastMidpointToEndV != null)
                {
                    waypoints.Add(lastMidpointToEndV);
                }
            }
            catch (System.Exception e)
            {
                string startVertexAsString = startVertex == null ? "<invalidVertex>" : startVertex.ToString();
                string endVertexAsString = endVertex == null ? "<invalidVertex>" : endVertex.ToString();
                string errMsg = MessageConstants.BuildMessage(MessageConstants.ERR_PATHFINDING_FAILED, new object[2] { startVertexAsString, endVertexAsString });
                logger.LogError(e, errMsg);
                throw new System.ArgumentException(errMsg);
            }
            return waypoints;
        }

        private double CalculateDistance(double goalX, double goalY, double pointX, double pointY, DistanceMeasure distanceMeasure)
        {
            double distance;
            switch (distanceMeasure)
            {
                case DistanceMeasure.MANHATTAN:
                    distance = System.Math.Abs(goalX - pointX) + System.Math.Abs(goalY - pointY);
                    break;
                case DistanceMeasure.EUCLIDEAN:
                    distance = System.Math.Sqrt(System.Math.Pow(goalX - pointX, 2) + System.Math.Pow(goalY - pointY, 2));
                    break;
                default:
                    throw new System.ArgumentException(MessageConstants.BuildMessage(MessageConstants.ERR_UNSUPPORTED_DISTANCE_METRIC, distanceMeasure.ToString()));
            }
            return distance;
        }   
    
        private bool ExistsPath(int startTriangleIdx, int endTriangleIdx, DistanceMeasure distanceMeasure)
        {
            openList.Enqueue(InitSearchNode(startTriangleIdx, endTriangleIdx, distanceMeasure), 0);

            SearchNode currNode;
            while (openList.Count > 0)
            {
                currNode = openList.Dequeue();
                if (currNode.TriangleIdx == endTriangleIdx) return true;
                searchContentLookupDict[currNode.TriangleIdx].Examined = true;
                Expand(currNode, endTriangleIdx, distanceMeasure);
            }
            return false;
        }

        private void Expand(SearchNode currNode, int endTriangleIdx, DistanceMeasure heuristicMeasure)
        {
            SearchNodeContent current = searchContentLookupDict[currNode.TriangleIdx];
            foreach (var successorTriangleIdx in searchContentLookupDict[currNode.TriangleIdx].Successors)
            {
                if (successorTriangleIdx == Constants.UNBOUNDED_FACE_INDEX) continue;
                SearchNode successorSearchNode = InitSearchNode(successorTriangleIdx, endTriangleIdx, heuristicMeasure);
                SearchNodeContent successor = searchContentLookupDict[successorTriangleIdx];
                // Successor was already expanded
                if (successor.Examined) continue;

                Triangle adjacentTriangle = triangulation.Triangles.Get(successorTriangleIdx);
                (double aTriangleMidPointX, double aTriangleMidPointY) = CalculateTriangleMidPoint(adjacentTriangle);
                (double currTriangleMidePointX, double currTriangleMidPointY) = CalculateTriangleMidPoint(triangulation.Triangles.Get(currNode.TriangleIdx));

                float cost = (float) (current.Cost + CalculateDistance(aTriangleMidPointX, aTriangleMidPointY, currTriangleMidePointX, currTriangleMidPointY, heuristicMeasure));
                // Successor is already in open list and current path to successor is shorter path from current node to successor
                if (openList.Contains(successorSearchNode) && cost >= successor.Cost) continue;

                successor.PrevIdx = currNode.TriangleIdx;
                successor.Cost = cost;

                if (openList.Contains(successorSearchNode))
                {
                    openList.UpdatePriority(successorSearchNode, cost);
                }
                else
                {
                    openList.Enqueue(successorSearchNode, cost);
                }
            }
        }
    
        private SearchNode InitSearchNode(int triangleIdx, int destinationTriangleIdx, DistanceMeasure heuristicMeasure)
        {
            if (searchContentLookupDict.ContainsKey(triangleIdx))
            {
                return new SearchNode(triangleIdx);
            }

            Triangle triangle = triangulation.Triangles.Get(triangleIdx);
            Triangle destination = triangulation.Triangles.Get(destinationTriangleIdx);

            int[] adjacentTriangles = new int[3];
            int idx = 0;
            foreach (var edge in triangle.Edges)
            {
                adjacentTriangles[idx++] = triangle.AdjacentTriangleForEdge(edge);
            }
            (double aTriangleMidPointX, double aTriangleMidPointY) = CalculateTriangleMidPoint(destination);
            (double currTriangleMidePointX, double currTriangleMidPointY) = CalculateTriangleMidPoint(triangle);

            SearchNodeContent searchNodeContent = new SearchNodeContent(adjacentTriangles, CalculateDistance(aTriangleMidPointX, aTriangleMidPointY, currTriangleMidePointX, currTriangleMidPointY, heuristicMeasure));
            SearchNode searchNode = new SearchNode(triangleIdx); 
            searchContentLookupDict.Add(triangleIdx, searchNodeContent);
            return searchNode;
        }

        private (double, double) CalculateTriangleMidPoint(Triangle t)
        {
            double midPointX = 0;
            double midPointY = 0;
            foreach (var vertexIdx in t.Vertices)
            {
                Vertex currTriangleVertex = triangulation.Vertices.Get(vertexIdx);
                midPointX += currTriangleVertex.X;
                midPointY += currTriangleVertex.Y;
            }
            return (midPointX/3, midPointY/3);
        }

        private Direction AngleToDirection(double angle)
        {
            if (angle < 0 || angle > 360) throw new System.ArgumentException(MessageConstants.BuildMessage(MessageConstants.ERR_NO_DIRECTION_FOR_ANGLE, angle));
            Direction direction = Direction.NONE;
            switch (angle)
            {
                case double val when val >= 0 && val < 15:
                    direction = Direction.EAST;
                    break;
                case double val when val >= 15 && val < 75:
                    direction = Direction.NORTH_EAST;
                    break;
                case double val when val >= 75 && val < 105:
                    direction = Direction.NORTH;
                    break;
                case double val when val >= 105 && val < 165:
                    direction = Direction.NORTH_WEST;
                    break;
                case double val when val >= 165 && val < 195:
                    direction = Direction.WEST;
                    break;
                case double val when val >= 195 && val < 255:
                    direction = Direction.SOUTH_WEST;
                    break;
                case double val when val >= 255 && val < 285:
                    direction = Direction.SOUTH;
                    break;
                case double val when val >= 285 && val < 345:
                    direction = Direction.SOUTH_EAST;
                    break;
                case double val when val >= 345 && val < 360:
                    direction = Direction.EAST;
                    break;
                default:
                    break;
            }
            return direction;
        }

        // https://stackoverflow.com/questions/21483999/using-atan2-to-find-angle-between-two-vectors/21484228#21484228
        // https://stackoverflow.com/questions/9970281/java-calculating-the-angle-between-two-points-in-degrees?noredirect=1&lq=1
        private double AngleBetweenPoints(double originX, double originY, double goalX, double goalY)
        {
            // (East-Counterclockwise Convention) = atan2(y, x)
            double theta_radiants = System.Math.Atan2(goalY - originY, goalX - originX);
            // Since theta returned by the built in atan2 is in the range [-pi/pi], add pi if theta < 0 to map to range [0°,360°]
            if (theta_radiants < 0)
            {
                theta_radiants += System.Math.PI;
            }
            return theta_radiants * (180 / System.Math.PI);
        }
    
        private Waypoint CreateWaypoint(double fromX, double fromY, double toX, double toY)
        {
            double distance = CalculateDistance(toX, toY, fromX, fromY, DistanceMeasure.EUCLIDEAN);
            Direction direction = AngleToDirection(AngleBetweenPoints(fromX, fromY, toX, toY));
            if (direction == Direction.NONE || distance == 0) return null;
            return new Waypoint(direction, distance);
        }
    }
}
