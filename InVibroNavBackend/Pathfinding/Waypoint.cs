﻿namespace InVibroNavBackend.Pathfinding
{
    public enum Direction
    {
        NORTH,
        NORTH_EAST,
        EAST,
        SOUTH_EAST,
        SOUTH,
        SOUTH_WEST,
        WEST,
        NORTH_WEST,
        NONE
    }

    public class Waypoint
    {
        private double distance;

        public Direction Direction { get; set; }
        public double Distance 
        { 
            get { return distance; }
            set
            {
                if (value <= 0)
                {
                    throw new System.ArgumentException(MessageConstants.BuildMessage(MessageConstants.ERR_INVALID_DISTANCE, value));
                }
                distance = value;
            }
        }

        public Waypoint(Direction direction, double distance)
        {
            Direction = direction;
            Distance = distance;
        }
    }
}
