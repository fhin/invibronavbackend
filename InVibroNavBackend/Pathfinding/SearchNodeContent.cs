﻿using Priority_Queue;

namespace InVibroNavBackend.Pathfinding
{
    internal class SearchNode : FastPriorityQueueNode
    {
        public int TriangleIdx { get; set; }

        public SearchNode(int triangleIdx)
        {
            TriangleIdx = triangleIdx;
        }
    }

    internal class SearchNodeContent
    {
        public bool Examined { get; set; }
        public double Estimate { get; set; }
        public double Cost { get; set; }
        public int PrevIdx { get; set; }
        public int[] Successors { get; set; }

        public SearchNodeContent(int[] successorTrianglesIdx, double estimate)
        {
            Estimate = estimate;
            Cost = 0;
            PrevIdx = Constants.UNDEFINED_TRIANGLE_INDEX;
            if (successorTrianglesIdx.Length != 3)
            {
                throw new System.ArgumentException();
            }
            Successors = successorTrianglesIdx;
        }
    }
}
