﻿using InVibroNavBackend.Geometry;
using InVibroNavBackend.Triangulation;

namespace InVibroNavBackend.Pathfinding
{
    public enum DistanceMeasure
    {
        MANHATTAN,
        EUCLIDEAN
    }
    public interface IPathFinder
    {
        public System.Collections.Generic.ICollection<Waypoint> FindPath(Vertex startVertex, Vertex endVertex, DistanceMeasure distanceMeasure);
    }
}
