﻿namespace InVibroNavBackend
{
    public static class MessageConstants
    {
        internal static readonly string ERR_FILE_DOES_NOT_EXIST = "No file exists at the given location {0:s}";
        internal static readonly string ERR_INVALID_FILE_PATH = "Could not read configuration from given location {0:s}";

        internal static readonly string ERR_INVALID_HEADER_LINE = "Header line needs to have format: {0:s} <numVertices > 0> [<NumEdges >= 0>], but was {1:s}";
        internal static readonly string ERR_INVALID_VERTEX_DEFINITION = "Invalid vertex definition, needs to be defined as: v <xCoordinate> <yCoordinate>, but was {0:s}";
        internal static readonly string ERR_DUPLICATE_VERTEX_DEFINITON = "Given vertex with coordinates x:{0:d} and y:{1:d} was already defined !";
        internal static readonly string ERR_INVALID_EDGE_INVALID_FORMAT = "Invalid edge definition, needs to be defined as: e <StartVertexIndex> <EndVertexIndex>";
        internal static readonly string ERR_INVALID_EDGE_SAME_ENDPOINTS = "Invalid edge defintion, endpoints <Start>: {0:d} and <End> {1:d} are the same";
        internal static readonly string ERR_INVALID_EDGE_INVALID_VERTEX = "Invalid edge defintion, given vertex {0:d} was not found";
        internal static readonly string ERR_DUPLICATE_EDGE_DEFINITION = "Given edge with startpoint {0:d} and endpoint {1:d} was already defined !";

        internal static readonly string ERR_VERTEX_COUNT_MISSMATCH = "Config defined {0:d} but only parsed {1:d} vertices";
        internal static readonly string ERR_EDGE_COUNT_MISSMATCH = "Config defined {0:d} but only parsed {1:d} edges";
        internal static readonly string ERR_PARSING_CONFIG = "Error while parsing given file config, reason: {0:s}";
        internal static readonly string ERR_INIT_TRIANGULATOR = "Error while initializing triangluation";

        internal static readonly string ERR_INVALID_NUM_TRIANGLE_VERTICES = "A triangle can only have three vertices !";
        internal static readonly string ERR_INVALID_NUM_TRIANGLE_EDGES = "A triangle can only have three edges !";

        internal static readonly string ERR_INVALID_EDGE_OF_TRIANGLE = "Given triangles (edges: {0:s}) does not contain edge with idx {1:d}";
        internal static readonly string ERR_UPDATING_ADJACENT_EDGE_TRIANGLE = "Error while update adjacent triangle for given edge with idx {0:d}";
        internal static readonly string ERR_EDGE_NOT_FOUND_IN_TRIANGLE = "Could not find edge with endpoints {0:d}-{1:d} in given triangle (edge: {2:s})";
        internal static readonly string ERR_FAILED_TRIANGULATION = "Failed triangulation of given data for reason: {0:s}, see inner exception for more details !";
        internal static readonly string ERR_EXPORTING_TRIANGULATION = "There was an error exporting the triangulation, see inner exception for more details";
        internal static readonly string ERR_EXPORT_LOCATION_NOT_DIRECTORY = "Given export location {0:s} was not a directory";

        #region Waypoint message constants
        internal static readonly string ERR_INVALID_DISTANCE = "Waypoint distance must be greater than 0 but was actually {0:d}";
        internal static readonly string ERR_NO_ENCLOSING_TRIANGLE_FOR_POINT = "Could not find enclosing triangle for point {0:s}";
        internal static readonly string ERR_PATHFINDING_FAILED = "There was an error while calculating the path from {0:s} to {1:s}";
        internal static readonly string ERR_UNSUPPORTED_DISTANCE_METRIC = "Given distance metric {0:s} is not supported !";
        internal static readonly string ERR_NO_PATH_FOUND = "Could not find a path";
        internal static readonly string ERR_NO_DIRECTION_FOR_ANGLE = "Can only determine direction for an angle within the range [0, 360] but was actually {0:d}";
        #endregion

        private static readonly string ERR_BUILDING_MSG = "There was an error building message with given format {0:s} !";

        public static string BuildMessage(string msgFormat, object data)
        {
            return BuildMessage(msgFormat, new object[1] { data });
        }

        public static string BuildMessage(string msgFormat, object[] data)
        {
            if (msgFormat == null)
            {
                msgFormat = "<null>";
            }
            try
            {
                return string.Format(msgFormat, data);
            }
            catch (System.FormatException e)
            {
                throw new System.ArgumentException(BuildMessage(ERR_BUILDING_MSG, msgFormat), e);
            }
            catch (System.ArgumentNullException e)
            {
                throw new System.ArgumentException(BuildMessage(ERR_BUILDING_MSG, msgFormat), e);
            }
        }
    }
}
