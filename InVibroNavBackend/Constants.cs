﻿namespace InVibroNavBackend
{
    internal static class Constants
    {
        public static readonly string DEF_CONFIG_LINE_PREFIX = "c";
        public static readonly string DEF_VERTEX_PREFIX = "v";
        public static readonly string DEF_EDGE_PREFIX = "e";
        public static readonly string DEF_TRIANGLE_PREFIX = "t";
        public static readonly string DEF_DEBUG_ADJ_TRIANGLE_FOR_EDGE_PREFIX = "a";

        public const int NOT_SET_VERTEX_INDEX = -1;
        public const int NOT_SET_EDGE_INDEX = -1;
        public const int UNBOUNDED_FACE_INDEX = -2;
        public const int UNDEFINED_TRIANGLE_INDEX = -1;

        public const int VERTEX_OUTSIDE_TRIANGLE = -1;
        public const int VERTEX_INSIDE_TRIANGLE = 1;
        public const int VERTEX_ON_BORDER_TRIANGLE = 0;
    }
}
