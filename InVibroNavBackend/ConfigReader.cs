﻿using InVibroNavBackend.Geometry;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("InVibroNavBackendConsole")]
namespace InVibroNavBackend
{
    public struct ConfigHeader
    {
        public int VertexCount { get; set; }
        public int EdgeCount { get; set; }
    }

    internal class ConfigReader
    {
        private string dataFilePath;
        private readonly ILogger logger;

        public string FilePath
        {
            get { return dataFilePath; }
            private set
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(value))
                    {
                        value = "<nullEmptyOrWhiteSpace>";
                    }
                    if (!File.Exists(value))
                    {
                        throw new ArgumentException(MessageConstants.BuildMessage(MessageConstants.ERR_FILE_DOES_NOT_EXIST, value));
                    }
                    dataFilePath = value;
                }
                catch (ArgumentException e)
                {
                    string errMsg = MessageConstants.BuildMessage(MessageConstants.ERR_INVALID_FILE_PATH, value);
                    logger.LogError(e, errMsg);
                    throw new ArgumentException(errMsg);
                }
                catch (NullReferenceException)
                {
                    string errMsg = MessageConstants.BuildMessage(MessageConstants.ERR_INVALID_FILE_PATH, value);
                    logger.LogError(errMsg);
                    throw new ArgumentException(errMsg);
                }
            }
        }

        public ConfigReader(ILogger logger)
        {
            dataFilePath = string.Empty;
            this.logger = logger;
        }

        public (GeometricEntityContainer<Vertex>, GeometricEntityContainer<Edge>) Read(string filePath)
        {
            try
            {
                FilePath = filePath;
                using (FileStream fs = File.OpenRead(FilePath))
                {
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        ConfigHeader header = ParseConfigHeader(sr.ReadLine());
                        if (header.VertexCount == 0)
                        {
                            return (new GeometricEntityContainer<Vertex>(0, logger), new GeometricEntityContainer<Edge>(0, logger));
                        }

                        IDictionary<int, int> symbolicVertices = new Dictionary<int, int>();
                        GeometricEntityContainer<Vertex> vertexContainer = new GeometricEntityContainer<Vertex>(header.VertexCount, logger);
                        GeometricEntityContainer<Edge> edgeContainer = new GeometricEntityContainer<Edge>(header.EdgeCount, logger);

                        while (!sr.EndOfStream)
                        {
                            string dataLine = sr.ReadLine();
                            if (dataLine.Length == 0) continue;
                            if (dataLine.StartsWith(Constants.DEF_VERTEX_PREFIX))
                            {
                                ParseVertexDefinition(dataLine, ref vertexContainer, ref symbolicVertices);
                            }
                            else if (dataLine.StartsWith(Constants.DEF_EDGE_PREFIX))
                            {
                                ParseEdgeDefinition(dataLine, vertexContainer.GetEntryCount(), ref edgeContainer, ref symbolicVertices);
                            }
                        }

                        if (vertexContainer.GetEntryCount() != header.VertexCount)
                        {
                            string errMsg = MessageConstants.BuildMessage(MessageConstants.ERR_VERTEX_COUNT_MISSMATCH, new object[] { header.VertexCount, vertexContainer.GetEntryCount() });
                            logger.LogError(errMsg);
                            throw new ArgumentException(errMsg);
                        }
                        if (edgeContainer.GetEntryCount() != header.EdgeCount)
                        {
                            string errMsg = MessageConstants.BuildMessage(MessageConstants.ERR_EDGE_COUNT_MISSMATCH, new object[] { header.EdgeCount, edgeContainer.GetEntryCount() });
                            logger.LogError(errMsg);
                            throw new ArgumentException(errMsg);
                        }
                        return (vertexContainer, edgeContainer);
                    }
                }
            }
            catch (Exception e)
            {
                string errMsg = MessageConstants.BuildMessage(MessageConstants.ERR_PARSING_CONFIG, e.Message);
                logger.LogError(errMsg);
                throw new ArgumentException(errMsg, e);
            }
        }

        private ConfigHeader ParseConfigHeader(string headerLine)
        {
            if (string.IsNullOrWhiteSpace(headerLine))
            {
                headerLine = "<nullEmptyOrWhiteSpace>";
            }
            bool isValid = false;
            int verticesCount = 0;
            int edgeCount = 0;

            if (headerLine.StartsWith(Constants.DEF_CONFIG_LINE_PREFIX))
            {
                try
                {
                    string[] data = headerLine.Substring(2, headerLine.Length - 2).Split(" ");
                    if (data.Length == 1)
                    {
                        isValid |= int.TryParse(data[0], out verticesCount);
                        isValid &= verticesCount > 0;
                    }
                    else if (data.Length == 2)
                    {
                        isValid |= int.TryParse(data[0], out verticesCount);
                        isValid &= verticesCount > 0;
                        isValid &= int.TryParse(data[1], out edgeCount);
                        isValid &= verticesCount >= 0;
                    }
                }
                catch (ArgumentOutOfRangeException) { }
            }
            if (!isValid)
            {
                throw new ArgumentException(MessageConstants.BuildMessage(MessageConstants.ERR_INVALID_HEADER_LINE, new object[2] { Constants.DEF_CONFIG_LINE_PREFIX, headerLine }));
            }
            return new ConfigHeader()
            {
                VertexCount = verticesCount,
                EdgeCount = edgeCount
            };
        }

        private void ParseVertexDefinition(string vertexLine, ref GeometricEntityContainer<Vertex> vertices, ref IDictionary<int, int> symbolicVertices)
        {
            if (string.IsNullOrWhiteSpace(vertexLine))
            {
                vertexLine = "<nullEmptyOrWhiteSpace>";
            }
            if (vertexLine.StartsWith(Constants.DEF_VERTEX_PREFIX))
            {
                try
                {
                    string[] data = vertexLine.Substring(2, vertexLine.Length - 2).Split(" ");
                    double xCoord = 0;
                    double yCoord = 0;

                    if (data.Length == 2 && double.TryParse(data[0], out xCoord) && double.TryParse(data[1], out yCoord))
                    {
                        Vertex newVertex = new Vertex(xCoord, yCoord);
                        if (vertices.ContainsEntity(newVertex))
                        {
                            string infoMsg = MessageConstants.BuildMessage(MessageConstants.ERR_DUPLICATE_VERTEX_DEFINITON, new object[2] { xCoord, yCoord });
                            logger.LogInformation(infoMsg);
                            // Store duplicate vertex with link to already stored vertex
                            symbolicVertices.Add(vertices.GetEntryCount() + symbolicVertices.Count, vertices.GetIndex(newVertex));
                        }
                        else
                        {
                            vertices.InsertEntity(newVertex);
                        }
                    }
                }
                catch (ArgumentOutOfRangeException) { }
            }
            else
            {
                throw new ArgumentException(MessageConstants.BuildMessage(MessageConstants.ERR_INVALID_VERTEX_DEFINITION, vertexLine));
            }
        }

        private void ParseEdgeDefinition(string edgeLine, int numDefinedVertices, ref GeometricEntityContainer<Edge> edges, ref IDictionary<int, int> symbolicVertices)
        {
            if (string.IsNullOrWhiteSpace(edgeLine))
            {
                edgeLine = "<nullEmptyOrWhiteSpace>";
            }
            if (edgeLine.StartsWith(Constants.DEF_EDGE_PREFIX))
            {
                try
                {
                    string[] data = edgeLine.Substring(2, edgeLine.Length - 2).Split(" ");
                    int sVertexIdx = 0;
                    int eVertexIdx = 0;
                    string errs = string.Empty;
                    if (data.Length == 2 && int.TryParse(data[0], out sVertexIdx) && int.TryParse(data[1], out eVertexIdx))
                    {
                        if (sVertexIdx == eVertexIdx)
                        {
                            errs += MessageConstants.BuildMessage(MessageConstants.ERR_INVALID_EDGE_SAME_ENDPOINTS, new object[2] { sVertexIdx, eVertexIdx }) + "\n";
                        }
                        if (sVertexIdx > numDefinedVertices || sVertexIdx < 0)
                        {
                            errs += MessageConstants.BuildMessage(MessageConstants.ERR_INVALID_EDGE_INVALID_VERTEX, sVertexIdx) + "\n";
                        }
                        if (eVertexIdx > numDefinedVertices || eVertexIdx < 0)
                        {
                            errs += MessageConstants.BuildMessage(MessageConstants.ERR_INVALID_EDGE_INVALID_VERTEX, eVertexIdx) + "\n";
                        }
                    }
                    if (errs.Length != 0)
                    {
                        throw new ArgumentException(MessageConstants.BuildMessage(MessageConstants.ERR_INVALID_VERTEX_DEFINITION, edgeLine) + "\n" + errs);
                    }
                    else
                    {
                        if (symbolicVertices.ContainsKey(sVertexIdx))
                        {
                            sVertexIdx = symbolicVertices[sVertexIdx];
                        }
                        if (symbolicVertices.ContainsKey(eVertexIdx))
                        {
                            eVertexIdx = symbolicVertices[eVertexIdx];
                        }

                        Edge newEdge = new Edge(sVertexIdx, eVertexIdx);
                        if (edges.ContainsEntity(newEdge))
                        {
                            string infoMsg = MessageConstants.BuildMessage(MessageConstants.ERR_DUPLICATE_EDGE_DEFINITION, new object[2] { sVertexIdx, eVertexIdx });
                            logger.LogInformation(infoMsg);
                        }
                        else
                        {
                            edges.InsertEntity(newEdge);
                        }
                    }
                }
                catch (ArgumentOutOfRangeException) { }
            }
            else
            {
                throw new ArgumentException(MessageConstants.BuildMessage(MessageConstants.ERR_INVALID_VERTEX_DEFINITION, edgeLine));
            }
        }
    }
}
