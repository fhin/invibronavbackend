﻿using System.Collections.Generic;

namespace InVibroNavBackend.Geometry
{
    public class Triangle
    {
        private IDictionary<int, int> adjacentTriangles;
        private ISet<int> vertices;
        private ISet<int> edges;

        public int Idx { get; }
        public ISet<int> Vertices 
        { 
            get { return vertices; } 
            set
            {
                if (value.Count != 3)
                {
                    throw new System.ArgumentException(MessageConstants.ERR_INVALID_NUM_TRIANGLE_VERTICES);
                }
                else
                {
                    vertices = value;
                }
            }
        }

        public ISet<int> Edges
        {
            get { return edges; }
            set
            {
                if (value.Count != 3)
                {
                    throw new System.ArgumentException(MessageConstants.ERR_INVALID_NUM_TRIANGLE_EDGES);
                }
                else
                {
                    edges = value;
                }
            }
        }

        public Triangle(int idx)
        {
            Idx = idx;
            vertices = new HashSet<int>(3);
            edges = new HashSet<int>(3);
            adjacentTriangles = new Dictionary<int, int>(3);
        }

        public int AdjacentTriangleForEdge(int edgeIdx)
        {
            if (!adjacentTriangles.ContainsKey(edgeIdx)) return Constants.NOT_SET_VERTEX_INDEX;
            return adjacentTriangles[edgeIdx];
        }
    
        public void SetAdjacentTriangleForEdge(int edgeIdx, int adjacentTriangleIdx)
        {
            if (!edges.Contains(edgeIdx))
            {
                throw new System.ArgumentException(MessageConstants.BuildMessage(MessageConstants.ERR_INVALID_EDGE_OF_TRIANGLE, new object[2] { string.Join(",", edges), edgeIdx }));
            }
            if (adjacentTriangles.ContainsKey(edgeIdx))
            {
                adjacentTriangles[edgeIdx] = adjacentTriangleIdx;
            }
            else
            {
                if (adjacentTriangles.Count > 3)
                {
                    throw new System.ArgumentException(MessageConstants.BuildMessage(MessageConstants.ERR_UPDATING_ADJACENT_EDGE_TRIANGLE, new object[1] { edgeIdx }));
                }
                adjacentTriangles.Add(edgeIdx, adjacentTriangleIdx);
            }
        }

        // TODO: error messages and check for duplicate keys
        public void UpdateAdjacentTriangleForEdge(int oldEdgeIdx, int newEdgeIdx, int adjacentTriangleIdx)
        {
            if (!edges.Contains(newEdgeIdx))
            {
                throw new System.ArgumentException(MessageConstants.BuildMessage(MessageConstants.ERR_INVALID_EDGE_OF_TRIANGLE, new object[2] { string.Join(",", edges), newEdgeIdx }));
            }
            if (adjacentTriangles.ContainsKey(oldEdgeIdx))
            {
                adjacentTriangles.Remove(oldEdgeIdx);
                adjacentTriangles.Add(newEdgeIdx, adjacentTriangleIdx);
            }
        }

        public int Contains(int vertexIdx, GeometricEntityContainer<Vertex> vertices, GeometricEntityContainer<Edge> edges)
        {
            return Contains(vertices.Get(vertexIdx), vertices, edges);
        }

        // TODO: Triangle vertices need to be either in clockwise or counterclockwise order:
        // see: https://stackoverflow.com/a/20861130
        // Determine if an arbitrary point lies inside of triangle
        // see either: https://math.stackexchange.com/a/51459
        // or a more optimized version: https://www.cs.cmu.edu/~quake/robust.html 
        public int Contains(Vertex v, GeometricEntityContainer<Vertex> vertices, GeometricEntityContainer<Edge> edges)
        {
            Vertex[] triangleVertices = new Vertex[3];
            int vertexPos = 0;
            foreach (var triangleVertexIdx in Vertices)
            {
                triangleVertices[vertexPos++] = vertices.Get(triangleVertexIdx);
            }
            Vertex A = triangleVertices[0];
            Vertex B = triangleVertices[1];
            Vertex C = triangleVertices[2];

            bool isLeftOfAB = (A.X - v.X) * (B.Y - v.Y) - (A.Y - v.Y) * (B.X - v.X) >= 0;
            bool isLeftOfAC = (A.X - v.X) * (C.Y - v.Y) - (A.Y - v.Y) * (C.X - v.X) >= 0;
            if (isLeftOfAB & isLeftOfAC) return Constants.VERTEX_OUTSIDE_TRIANGLE;

            bool isLeftOfBC = (B.X - v.X) * (C.Y - v.Y) - (B.Y - v.Y) * (C.X - v.X) >= 0;
            if (!isLeftOfBC) return Constants.VERTEX_OUTSIDE_TRIANGLE;

            if (GetEdgeContainingVertex(v, edges, vertices) != Constants.NOT_SET_EDGE_INDEX) return Constants.VERTEX_ON_BORDER_TRIANGLE;
            return Constants.VERTEX_INSIDE_TRIANGLE;
        }

        public int VertexInTriangleSpannedCircle(Vertex v, Vertex v1, Vertex v2, Vertex v3)
        {
            return InCircleTest(v.X, v.Y, v1.X, v1.Y, v2.X, v2.Y, v3.X, v3.Y);
        }

        // More robust and efficient predicate can be found @ https://www.cs.cmu.edu/~quake/robust.html
        // A 2D variant can be found @ https://stackoverflow.com/questions/481144/equation-for-testing-if-a-point-is-inside-a-circle
        // d = v
        private int InCircleTest(double vx, double vy, double ax, double ay, double bx, double by, double cx, double cy)
        {
            double equivI = (ax - vx) * (by - vy) * System.Math.Pow(cx - vx, 2) + System.Math.Pow(cy - vy, 2);
            double equivII = (ay - vy) * System.Math.Pow(bx - vx, 2) * System.Math.Pow(by - vy, 2) * (cx - vx);
            double equivIII = System.Math.Pow(ax - vx, 2) + System.Math.Pow(ay - vy, 2) * (bx - vx) * (by - vy);
            double equivIV = (cx - vx) * (by - vy) * System.Math.Pow(ax - vx, 2) + System.Math.Pow(ay - vy, 2);
            double equivV = (cy - vy) * System.Math.Pow(bx - vx, 2) + System.Math.Pow(by - vy, 2) * (ax - vx);
            double equivVI = System.Math.Pow(cx - vx, 2) + System.Math.Pow(cy - vy, 2) * (bx - vx) * (ay - vy);

            double testValue = equivI + equivII + equivIII - (equivIV + equivV + equivVI);
            if (testValue == 0)
            {
                return Constants.VERTEX_ON_BORDER_TRIANGLE;
            }
            else if (testValue > 0)
            {
                return Constants.VERTEX_INSIDE_TRIANGLE;
            }
            else
            {
                return Constants.VERTEX_OUTSIDE_TRIANGLE;
            }
        }

        // Test for colinearity can be found @ http://bit-player.org/wp-content/extras/bph-publications/BeautifulCode-2007-Hayes.pdf
        // Calucate cross product of vectors a-b and b-x where a and b are the vertices of the edge
        // If cross product = 0, points are colinear
        // Test whether x lies between a and b or is one of the edges vertices
        public int GetEdgeContainingVertex(Vertex vertex, GeometricEntityContainer<Edge> edges, GeometricEntityContainer<Vertex> vertices)
        {
            int[] edgeIdxs = new int[3];
            Edges.CopyTo(edgeIdxs, 0);

            foreach (var edgeIdx in edgeIdxs)
            {
                Edge e = edges.Get(edgeIdx);
                Vertex edgeStartVertex = vertices.Get(e.StartVertexIdx);
                Vertex edgeEndVertex = vertices.Get(e.EndVertexIdx);

                if (DoesVertexLieOnEdge(vertex, edgeStartVertex, edgeEndVertex)) return edgeIdx;
            }
            return Constants.NOT_SET_EDGE_INDEX;
        }

        private bool DoesVertexLieOnEdge(Vertex vertex, Vertex edgeStartVertex, Vertex edgeEndVertex)
        {
            double vec_ab_x = edgeEndVertex.X - edgeStartVertex.X;
            double vec_ab_y = edgeEndVertex.Y - edgeStartVertex.Y;
            double vec_bx_x = edgeEndVertex.X - vertex.X;
            double vec_bx_y = edgeEndVertex.Y - vertex.Y;

            /*
             *  (a_x, a_y, a_z) x (b_x, b_y, b_z) = (a_y*b_z - a_z*b_y, a_z*b_x-a_x*b_z, a_x*b_y - a_y*b_x) 
             */
            double crossProduct = vec_ab_x * vec_bx_y - vec_ab_y * vec_bx_x;
            if (crossProduct != 0) return false;

            double minX = System.Math.Min(edgeStartVertex.X, edgeEndVertex.X);
            double maxX = System.Math.Max(edgeStartVertex.X, edgeEndVertex.X);

            if (vertex.X < minX || vertex.X > maxX) return false;

            double minY = System.Math.Min(edgeStartVertex.Y, edgeEndVertex.Y);
            double maxY = System.Math.Min(edgeStartVertex.Y, edgeEndVertex.Y);

            if (vertex.Y < minY || vertex.Y > maxY) return false;
            return true;
        }

        public int GetEdgeIdx(int endpointIdx1, int endpointIdx2, GeometricEntityContainer<Edge> edges)
        {
            int matchingEdgeIdx = -1;
            foreach (var edgeIdx in this.edges)
            {
                Edge edge = edges.Get(edgeIdx);
                if ((edge.StartVertexIdx == endpointIdx1 && edge.EndVertexIdx == endpointIdx2)
                    || (edge.StartVertexIdx == endpointIdx2 && edge.EndVertexIdx == endpointIdx1))
                {
                    matchingEdgeIdx = edgeIdx;
                    break;
                }
            }
            if (matchingEdgeIdx == -1)
            {
                throw new System.ArgumentException(MessageConstants.BuildMessage(MessageConstants.ERR_EDGE_NOT_FOUND_IN_TRIANGLE, new object[3] { endpointIdx1, endpointIdx2, string.Join(",", edges) }));
            }
            return matchingEdgeIdx;
        }

        public override string ToString()
        {
            return Constants.DEF_TRIANGLE_PREFIX + " " + string.Join(" ", Vertices);
        }

        public string ToDebugString()
        {
            string debugString = ToString() + "\n";
            debugString += Constants.DEF_EDGE_PREFIX + " " + string.Join(" ", Edges) + "\n";
            foreach (var edge in Edges)
            {
                debugString += Constants.DEF_DEBUG_ADJ_TRIANGLE_FOR_EDGE_PREFIX + " " + edge + " " + adjacentTriangles[edge] + "\n";
            }
            return debugString;
        }
    }
}
