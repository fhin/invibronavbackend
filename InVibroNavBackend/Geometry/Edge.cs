﻿namespace InVibroNavBackend.Geometry
{
    public class Edge
    {
        public int StartVertexIdx { get; set; }
        public int EndVertexIdx { get; set; }

        public Edge(int sVertexIdx, int eVertexIdx)
        {
            StartVertexIdx = sVertexIdx;
            EndVertexIdx = eVertexIdx;
        }

        public override int GetHashCode()
        {
            //return System.HashCode.Combine(StartVertexIdx, EndVertexIdx);
            return StartVertexIdx + EndVertexIdx;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Edge)) return false;
            return Equals((Edge)obj);
        }

        private bool Equals(Edge other)
        {
            return (StartVertexIdx == other.StartVertexIdx && EndVertexIdx == other.EndVertexIdx)
                || (StartVertexIdx == other.EndVertexIdx && EndVertexIdx == other.StartVertexIdx);
        }

        public override string ToString()
        {
            return Constants.DEF_EDGE_PREFIX + " " + StartVertexIdx + " " + EndVertexIdx;
        }
    }
}
