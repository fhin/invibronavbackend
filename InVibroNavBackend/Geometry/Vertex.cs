﻿namespace InVibroNavBackend.Geometry
{
    public class Vertex : System.IComparable
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Vertex(double x, double y)
        {
            X = x;
            Y = y;
        }

        public override int GetHashCode()
        {
            return System.HashCode.Combine(X, Y);
        }

        public override string ToString()
        {
            return Constants.DEF_VERTEX_PREFIX + " " + X + " " + Y;
        }

        public int CompareTo(object obj)
        {
            if (!(obj is Vertex)) throw new System.ArgumentException();
            Vertex other = (Vertex)obj;

            if (Y > other.Y || Y == other.Y && X < other.X)
            {
                return -1;
            }
            else if (Y == other.Y && X == other.X)
            {
                return 0;
            }
            return 1;
        }
    }
}
