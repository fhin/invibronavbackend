﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace InVibroNavBackend.Geometry
{
    public class GeometricEntityContainer<T>
    {
        private IDictionary<T, int> lookupDict;
        private IList<T> storedEntities;
        private readonly ILogger logger;

        public GeometricEntityContainer(ILogger logger)
            : this (-1, logger)
        {
        }

        public GeometricEntityContainer(int capacity, ILogger logger)
        {
            if (capacity <= 0)
            {
                lookupDict = new Dictionary<T, int>();
                storedEntities = new List<T>();
            }
            else
            {
                lookupDict = new Dictionary<T, int>(capacity);
                storedEntities = new List<T>(capacity);
            }
            this.logger = logger;
        }

        public bool ContainsEntity(T entityToLookup)
        {
            try
            {
                return lookupDict.ContainsKey(entityToLookup);
            }
            catch (NullReferenceException) { return false; }
        }

        public int InsertEntity(T entity)
        {
            if (ContainsEntity(entity)) return lookupDict[entity];
            try
            {
                int newEntityIdx = lookupDict.Count;
                lookupDict.Add(entity, newEntityIdx);
                storedEntities.Add(entity);
                return newEntityIdx;
            }
            catch (NullReferenceException)
            {
                // TODO: Logging
            }
            return -1;
        }

        public T Get(int entityPosition)
        {
            try
            {
                return storedEntities[entityPosition];
            }
            catch (IndexOutOfRangeException)
            {
                throw new ArgumentException();
                // TODO: Logging
            }
        }

        public void Remove(T entity)
        {
            try
            {
                int entityIdx = lookupDict[entity];
                lookupDict.Remove(entity);
                storedEntities.RemoveAt(entityIdx);
            }
            catch (NotSupportedException)
            {
                // TODO: Logging
                throw new ArgumentException();
            }
            catch (ArgumentOutOfRangeException)
            {
                // TODO: Logging
                throw new ArgumentException();
            }
            catch (ArgumentNullException)
            {
                // TODO: Logging
                throw new ArgumentException();
            }
        }

        public void Update(T oldEntity, T entity)
        {
            try
            {
                if (!ContainsEntity(oldEntity))
                {
                    throw new ArgumentException();
                }
                int idx = lookupDict[oldEntity];
                lookupDict.Remove(oldEntity);
                lookupDict.Add(entity, idx);
                storedEntities[idx] = entity;
            }
            catch (ArgumentException e)
            {
                throw new ArgumentException();
            }
        }

        public int GetIndex(T entity)
        {
            int idx = -1;
            try
            {
                lookupDict.TryGetValue(entity, out idx);
            }
            catch (ArgumentNullException) { }
            return idx;
        }

        public int GetEntryCount()
        {
            return lookupDict.Keys.Count;
        }
   
        public ICollection<T> GetAll()
        {
            return storedEntities;
        }
    }
}
